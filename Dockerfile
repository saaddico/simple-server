FROM golang:1.13-alpine as builder

# set environment variables
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

# set a working directory
WORKDIR /build

# download dependencies
COPY go.mod .
RUN go mod download

# Copy the code into the container
COPY . .

# build application
RUN go build -o main .


WORKDIR /dist

# Copy binary from build to main folder
RUN cp /build/main .


# Build a small image
FROM scratch

COPY --from=builder /dist/main /

EXPOSE 8080

# Command to run
ENTRYPOINT ["/main"]