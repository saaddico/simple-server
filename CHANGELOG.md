# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.3.4](https://bitbucket.org/saaddico/simple-server/compare/v1.3.3...v1.3.4) (2021-01-04)


### Bug Fixes

* **usres:** User login ([13eda16](https://bitbucket.org/saaddico/simple-server/commit/13eda166262b0d99a3303f31b0fcec45d0a46bc4))

### [1.3.3](https://bitbucket.org/saaddico/simple-server/compare/v1.3.0...v1.3.3) (2020-10-15)


### Bug Fixes

* **package json:** testing ([86fa817](https://bitbucket.org/saaddico/simple-server/commit/86fa817e91ebc3f30cd5efc89ccbd49919a603a1))
* **simple-server:** [#245](https://bitbucket.org/saaddico/simple-server/issues/245) minor update ([68f6a45](https://bitbucket.org/saaddico/simple-server/commit/68f6a45a74f5dad1e2e05c16e7b3ba29e6ad3a4c))

### [1.3.3](https://bitbucket.org/saaddico/simple-server/compare/v1.3.0...v1.3.3) (2020-10-15)


### Bug Fixes

* **simple-server:** [#245](https://bitbucket.org/saaddico/simple-server/issues/245) minor update ([68f6a45](https://bitbucket.org/saaddico/simple-server/commit/68f6a45a74f5dad1e2e05c16e7b3ba29e6ad3a4c))

## [1.3.0](https://bitbucket.org/saaddico/simple-server/compare/v1.2.0...v1.3.0) (2020-10-15)


### Features

* **simple-server:** [#453](https://bitbucket.org/saaddico/simple-server/issues/453) updated package json ([8ad8965](https://bitbucket.org/saaddico/simple-server/commit/8ad89650b889d380e108a5798c1433a959462961))


### Bug Fixes

* **simple-server:** [#123](https://bitbucket.org/saaddico/simple-server/issues/123) fix to package json ([822beb8](https://bitbucket.org/saaddico/simple-server/commit/822beb8d11a34cec2134f60626979c71914731ca))
* **simple-server:** [#231](https://bitbucket.org/saaddico/simple-server/issues/231) fix shit ([7ae0c12](https://bitbucket.org/saaddico/simple-server/commit/7ae0c12d0889ddefd93456c6bbf6292b3d4cc1ff))
* **simple-server:** [#242](https://bitbucket.org/saaddico/simple-server/issues/242) fix again ([453b74b](https://bitbucket.org/saaddico/simple-server/commit/453b74bd22983793fe00d213f55b2c781dadad92))
* **simple-server:** [#980](https://bitbucket.org/saaddico/simple-server/issues/980) fix again ([6aeebe6](https://bitbucket.org/saaddico/simple-server/commit/6aeebe66f3a9d9a3ea566f88a9a82e9202f4d969))

## [1.2.0](https://bitbucket.org/saaddico/simple-server/compare/v1.1.0...v1.2.0) (2020-10-15)


### Features

* **simple-server:** [#456](https://bitbucket.org/saaddico/simple-server/issues/456) update something ([9fdf7df](https://bitbucket.org/saaddico/simple-server/commit/9fdf7df59c194c780a0029b9945b357bd29bd57b))

## [1.1.0](https://bitbucket.org/saaddico/simple-server/compare/v1.0.0...v1.1.0) (2020-10-15)


### Features

* **simple-server:** [#34](https://bitbucket.org/saaddico/simple-server/issues/34) fixing issue ([f888a6f](https://bitbucket.org/saaddico/simple-server/commit/f888a6fa7765c25bd760fbdf00c8a632c9c01ee3))
* **simple-server:** [#433](https://bitbucket.org/saaddico/simple-server/issues/433) fixed bug ([9c8668c](https://bitbucket.org/saaddico/simple-server/commit/9c8668c3840a6f7746814ae822c8f4f18f9fed0a))
* **simple-server:** RIGD-43 changed server message ([191b18f](https://bitbucket.org/saaddico/simple-server/commit/191b18fa2402b8896413aca60f62815d467acd62))


### Bug Fixes

* **simple-server:** [#459](https://bitbucket.org/saaddico/simple-server/issues/459) updated ci file ([a9d5731](https://bitbucket.org/saaddico/simple-server/commit/a9d5731ae0d333a1d177c1dc0edcd97d3374df97))

## [1.1.0](https://bitbucket.org/saaddico/simple-server/compare/v1.0.0...v1.1.0) (2020-10-15)


### Features

* **simple-server:** [#34](https://bitbucket.org/saaddico/simple-server/issues/34) fixing issue ([f888a6f](https://bitbucket.org/saaddico/simple-server/commit/f888a6fa7765c25bd760fbdf00c8a632c9c01ee3))
* **simple-server:** [#433](https://bitbucket.org/saaddico/simple-server/issues/433) fixed bug ([9c8668c](https://bitbucket.org/saaddico/simple-server/commit/9c8668c3840a6f7746814ae822c8f4f18f9fed0a))
* **simple-server:** RIGD-43 changed server message ([191b18f](https://bitbucket.org/saaddico/simple-server/commit/191b18fa2402b8896413aca60f62815d467acd62))

## [1.1.0-beta.0](https://bitbucket.org/saaddico/simple-server/compare/v1.0.0...v1.1.0-beta.0) (2020-10-15)


### Features

* **simple-server:** [#34](https://bitbucket.org/saaddico/simple-server/issues/34) fixing issue ([f888a6f](https://bitbucket.org/saaddico/simple-server/commit/f888a6fa7765c25bd760fbdf00c8a632c9c01ee3))
* **simple-server:** RIGD-43 changed server message ([191b18f](https://bitbucket.org/saaddico/simple-server/commit/191b18fa2402b8896413aca60f62815d467acd62))

## 1.0.0 (2020-10-15)


### Features

* **main.go:** update server message ([947b079](https://bitbucket.org/saaddico/simple-server/commit/947b0796ac0bfcfb47b255ae686a526ec0740be2))
* **simple-server:** [#34](https://bitbucket.org/saaddico/simple-server/issues/34) manual bump ([e04ccbf](https://bitbucket.org/saaddico/simple-server/commit/e04ccbf3fabe51244d1ec75207ad67d47eb83c99))
* **simple-server:** [#45](https://bitbucket.org/saaddico/simple-server/issues/45) release reset ([ae06a5d](https://bitbucket.org/saaddico/simple-server/commit/ae06a5de5c352ec307790abace5864624df83dbc))
* **simple-server:** RIGD-43 manual bumping ([abf920a](https://bitbucket.org/saaddico/simple-server/commit/abf920a1925547c8234f564c9123dec92c997082))
* **simple-server:** RIGD-43 release reet ([389dfa0](https://bitbucket.org/saaddico/simple-server/commit/389dfa0412ce6538343740f8a9d35cf28e98ed58))
* **simple-server): #345 feat(simple-server:** [#34](https://bitbucket.org/saaddico/simple-server/issues/34) manual bump ([fe11d51](https://bitbucket.org/saaddico/simple-server/commit/fe11d51d91b057e8a4caad2557ae82b653c64e46)), closes [#345](https://bitbucket.org/saaddico/simple-server/issues/345)
* **simple-server): 3433# fix(simple-server): #45 feat(simple-server): #345 feat(simple-server:** [#34](https://bitbucket.org/saaddico/simple-server/issues/34) manual bump ([fcf1b75](https://bitbucket.org/saaddico/simple-server/commit/fcf1b755129a8ac53b8ff3f16743b6d7edfb2b85)), closes [#45](https://bitbucket.org/saaddico/simple-server/issues/45) [#345](https://bitbucket.org/saaddico/simple-server/issues/345)
* **user:** changes to user registration ([cee88d7](https://bitbucket.org/saaddico/simple-server/commit/cee88d77a6c0a3fd62c2ba14b514eb55844027da))


### Bug Fixes

* **simple-server:** [#23](https://bitbucket.org/saaddico/simple-server/issues/23) troubleshooting issue i am tired ([1829c62](https://bitbucket.org/saaddico/simple-server/commit/1829c62c3af0ed91b246f32824973a5453a13b85))
* **simple-server:** RIGD-42 manual bump ([70102c5](https://bitbucket.org/saaddico/simple-server/commit/70102c59d50c299d95f9a81c71ffa7ade9bbcde6))
* **simple-server:** RIGD-43 test ([ddf83c1](https://bitbucket.org/saaddico/simple-server/commit/ddf83c1bf4a23bf04b19cd9026fee12e3ea642ab))
* **simple-server): #45 feat(simple-server): #345 feat(simple-server:** [#34](https://bitbucket.org/saaddico/simple-server/issues/34) manual bump ([90e3348](https://bitbucket.org/saaddico/simple-server/commit/90e334802e254d5371e9ce3a03768293cc9ff347)), closes [#45](https://bitbucket.org/saaddico/simple-server/issues/45) [#345](https://bitbucket.org/saaddico/simple-server/issues/345)
* **simple-server): RIGD-43 fix(simple-server:** RIGD-43 manually bumps version ([78f5a93](https://bitbucket.org/saaddico/simple-server/commit/78f5a93cc1164243af5f680806ada22684803c9a))
* **testing:** testing ([5281153](https://bitbucket.org/saaddico/simple-server/commit/5281153ba34d253d4242351260a21830be066da9))
* **user:** update server message ([3c92f5b](https://bitbucket.org/saaddico/simple-server/commit/3c92f5b061e4cfb245508e3acf802813b9b7e61f))
